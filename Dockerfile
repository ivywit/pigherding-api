FROM node:13-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm i --only=production

COPY ./src ./src

EXPOSE 4000

CMD ["node", "./src/index.js"]
