const admin = require('firebase-admin');
const express = require('express');
const dotenv = require('dotenv');
const fetch = require('node-fetch');
const bodyParser = require('body-parser');
const cors = require('cors');
const { Storage } = require('@google-cloud/storage');
const { PubSub } = require('@google-cloud/pubsub');
const mime = require('mime-types');
const { v4: uuid } = require('uuid');

const app = express();
dotenv.config();
const {
  BASE_PATH,
  MAPS_API_KEY,
  SERVICE_ACCOUNT,
  IMAGE_SERVICE_ACCOUNT,
  BUCKET,
  PROJECT_ID
} = process.env;

admin.initializeApp({
  credential: admin.credential.cert(JSON.parse(SERVICE_ACCOUNT))
});

const db = admin.firestore();

const storage = new Storage({projectId: PROJECT_ID, credentials: JSON.parse(IMAGE_SERVICE_ACCOUNT)});
const bucket = storage.bucket(BUCKET);
const pubSub = new PubSub();

function prepareImages (name, files) {
  if (!files || files.length < 1) return null;
  const createdSource = [];

  // convert image and upload
  for (index in files) {
    const updatedName = name + '-' + index;
    const file = files[index];
    const mimeIndex = file.indexOf(';base64,');
    const mimeType = file.slice(5,mimeIndex);
    const ext = mime.extension(mimeType);
    createdSource.push(`https://${BASE_PATH}/footage/${updatedName}.${ext}`);
    const base64File = file.slice(mimeIndex + 8);
    const fileBuffer =  Buffer.from(base64File, 'base64');
    try {
      bucket.file(`footage/${updatedName}.${ext}`)
            .save(fileBuffer, {
              metadata: { contentType: mimeType },
              validation: 'md5'
            })
    } catch(error) {
      console.error(error);
    }
  }
  return createdSource;
}

app.use(cors());
app.use(bodyParser.urlencoded({limit: '50mb', extended: false}));
app.use(bodyParser.json({limit: '50mb'}));

app.get('/addresses/:lat/:lng', async (req, res) => {
  try {
    const { lat, lng } = req.params;
    const response = await fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${MAPS_API_KEY}`);
    const data = await response.json();
    const addressObject = data.results[0];
    const {
      formatted_address: address,
      address_components: components
    } = addressObject;
    const { long_name: city } = components.find(component => component.types[0] === 'locality');

    res.json({
      city: city.toLowerCase(),
      address
    });
    
  } catch (error) {
    console.error(error);
  }
});

app.post('/report', async (req, res) => {
  const { report } = req.body;
  const updatedReport = {
    ...report,
    files: prepareImages(uuid(), report.files)
  };
  async function publishMessage () {
    const topicName = `reports-${report.city}`;

    // Publishes the message as a string, e.g. "Hello, world!" or JSON.stringify(someObject)
    const dataBuffer = Buffer.from(JSON.stringify(updatedReport));
    const messageId = await pubSubClient.topic(topicName).publish(dataBuffer);
    console.log(`Message ${messageId} published.`);
  }
  
  const addedReport = await db.collection('report').add(updatedReport);
  publishMessage().catch(console.error);
  
  res.status(201).json({ report: addedReport.id });
});

app.listen(4000, () => {
  console.log('listening on port 4000');
});
